function hitungTotal() {
    const nama = document.getElementById("nama").value;
    const harga = parseFloat(document.getElementById("harga").value);
    const jumlahTiket = parseInt(document.getElementById("jumlahTiket").value);
    const member = document.getElementById("member").checked;
    const discount = parseFloat(document.getElementById("discount").value);

    let totalBayar = harga * jumlahTiket;

    if (member) {
        totalBayar *= 0.9; // 10% discount for members
    }

    totalBayar = totalBayar - (totalBayar * (discount / 100));

    document.getElementById("totalBayar").value = totalBayar.toFixed(2);

    // Display an alert with a success message
    const alertMessage = `Pemesanan untuk ${nama} berhasil!\nTotal Bayar: Rp ${totalBayar.toFixed(2)}`;
    window.alert(alertMessage);
}
